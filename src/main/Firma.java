package main;

import java.util.ArrayList;

public class Firma {

    private String adresa = "Strada Principala";
    private ArrayList<Angajat> angajati = new ArrayList<>();

    private Firma(){
    }

    private static final class SingletonHolder{
        public static final Firma INSTANCE = new Firma();
    }

    public static Firma getInstance(){
        return SingletonHolder.INSTANCE;
    }

    public void angajeaza(Angajat angajat){
        if (angajati.contains(angajat)){
            System.out.println("angajatul " + angajat.getNume() +" deja exista in firma");
        }else{
            angajati.add(angajat);
            System.out.println("a fost adaugat angajatul " + angajat.getNume());
        }
    }

    public void afiseazaAngajati(){
        for(Angajat a : angajati){
            System.out.println(a);
        }
    }

}
