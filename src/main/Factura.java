package main;

import java.util.ArrayList;

public class Factura {
    private double sumaTotala;
    private int nrFactura;
    private boolean platitDevreme = false;
    private boolean platit = false;

    private static ArrayList<Factura> facturi = new ArrayList<>();

    public double getSumaTotala() {
        return sumaTotala;
    }

    public void setSumaTotala(double sumaTotala) {
        this.sumaTotala = sumaTotala;
    }

    public int getNrFactura() {
        return nrFactura;
    }

    public void setNrFactura(int nrFactura) {
        this.nrFactura = nrFactura;
    }

    public boolean isPlatitDevreme() {
        return platitDevreme;
    }

    public void setPlatitDevreme(boolean platitDevreme) {
        this.platitDevreme = platitDevreme;
    }

    public boolean isPlatit() {
        return platit;
    }

    public void setPlatit(boolean platit) {
        this.platit = platit;
    }

    public Factura(double sumaTotala, int nrFactura) {
        this.sumaTotala = sumaTotala;
        this.nrFactura = nrFactura;

        facturi.add(this);

    }

    @Override
    public String toString() {
        return "Factura{" +
                "sumaTotala=" + sumaTotala +
                ", nrFactura=" + nrFactura +
                '}';
    }

    public static void afisareFacturiEmise(){
        for(Factura f : facturi){
            System.out.println(f);
        }
    }

}
