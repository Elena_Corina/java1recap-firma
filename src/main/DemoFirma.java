package main;

public class DemoFirma {
    public static void main(String[] args) {
        Firma firmaMea = Firma.getInstance();

        Angajat a1 = new Angajat.Builder().setNrAngajat(1).setNume("Angajatul1")
                .setEmail("angajatul1@firmaMea.com").setVarsta(24).build();

        Angajat a2 = new Angajat.Builder().setNrAngajat(2).setNume("Angajatul2")
                .setEmail("angajatul2@firmaMea.com").setVarsta(33).build();

        Angajat a3 = new Angajat.Builder().setNrAngajat(3).setNume("Angajatul3")
                .setEmail("angajatul3@firmaMea.com").setVarsta(31).build();

       System.out.println(a1);
       System.out.println(a2);
       System.out.println(a3);

        Client c1 = new Client("client1", "client1@firmaMea.com", 53);
        Client c2 = new Client("client2", "client2@firmaMea.com", 42);
        Client c3 = new Client("client3", "client3@firmaMea.com", 23);
        Client c4 = new Client("client4", "client4@firmaMea.com", 36);
        Client c5 = new Client("client5", "client5@firmaMea.com", 32);

        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        System.out.println(c4);
        System.out.println(c5);

       firmaMea.angajeaza(a1);
       firmaMea.angajeaza(a2);
       firmaMea.afiseazaAngajati();
       firmaMea.angajeaza(a1);

       a1.emiteFacturaDigi(200,15);
       a2.emiteFacturaEnel(150, 7);


        Factura f1 = new Factura(304.12, 1);
        Factura f2 = new Factura(123.43, 2);
        Factura f3 = new FacturaDigi(234, 3);
        FacturaDigi f4 = new FacturaDigi(222.22, 4);
        FacturaEnel f5 = new FacturaEnel(313.2, 5);

           Factura.afisareFacturiEmise();

       try {
            c1.platesteFactura(f1, true);
            c2.platesteFactura(f2, true);
            c3.platesteFactura(f3, true);
            c4.platesteFactura(f4, false);
            c5.platesteFactura(f5, false);
        }catch(PreaMulteFacturiException e){
           e.printStackTrace();
       }

        Factura f6 = new Factura(444, 6);
        Factura.afisareFacturiEmise();

      try {
           c5.platesteFactura(f6, false);
      } catch (PreaMulteFacturiException e) {
            e.printStackTrace();
    }

    }
}

