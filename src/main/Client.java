package main;

public class Client extends Persoana {

    private static int contor = 100;
    private int nrClient;
    private Factura [] listaFacturiPlatite = new Factura [5];

    public Client(String nume, String email, int varsta){
        super(nume, email, varsta);
        this.nrClient = contor;
        contor +=1;
    }

    public void platesteFactura (Factura factura, boolean platitDevreme) throws PreaMulteFacturiException{
        if (platitDevreme == true) {
            double discount = factura.getSumaTotala() * 0.10;
            double sumaCuDiscount = factura.getSumaTotala() - discount;
            factura.setSumaTotala(sumaCuDiscount);

            factura.setPlatit(true);
            for (int i = 0; i < listaFacturiPlatite.length; i++) {
                if (listaFacturiPlatite[i] == null) {
                    listaFacturiPlatite[i] = factura;
                    break;
                } else {
                    throw new PreaMulteFacturiException();
                }
            }
        } else {
            factura.setPlatit(true);
            for (int i = 0; i < listaFacturiPlatite.length; i++) {
                if (listaFacturiPlatite[i] == null) {
                    listaFacturiPlatite[i] = factura;
                    break;
                } else {
                    throw new PreaMulteFacturiException();
                }
            }

        }
    }

    public String toString() {
        return "Client{" +
                " nrClient= " + nrClient +
                " numeClient= " + getNume()+
                " emailClient= " + getEmail()+
                " varstaClient= " + getVarsta()+
                '}';
    }
}
