package main;

public class Angajat extends Persoana{

    private int nrAngajat;

    private Angajat(){
        super("","",0);
    }

    public int getNrAngajat() {
        return nrAngajat;
    }

    public static class Builder{
        Angajat INSTANCE = new Angajat();

        public Builder setNume(String nume){
           INSTANCE.setNume(nume);
           return this;
        }

        public Builder setEmail(String email){
            INSTANCE.setEmail(email);
            return this;
        }

        public Builder setVarsta(int varsta){
            INSTANCE.setVarsta(varsta);
            return this;
        }

        public Builder setNrAngajat(int nrAngajat) {
            INSTANCE.nrAngajat = nrAngajat;
            return this;
        }

        public Angajat build(){
            return INSTANCE;
        }
    }

    public FacturaDigi emiteFacturaDigi(double sumaTotala, int nrFactura){
        return new FacturaDigi(sumaTotala, nrFactura);
    }

    public FacturaDigi emiteFacturaEnel(double sumaTotala, int nrFactura){
        return new FacturaDigi(sumaTotala, nrFactura);
    }

    @Override
    public String toString() {
        return "Angajat{" +
                " nrAngajat=" + getNrAngajat()+
                " numeAngajat= " + getNume()+
                " emailAngajat= " + getEmail()+
                " varstaAngajat= " + getVarsta()+
                '}';
    }
}
